package com.oleksandr.streams;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntGenerator {
    private Stream<Integer> integerStream;
    private Random rand;

    public IntGenerator() {
        rand = new Random();
    }

    public List<Integer> generateList(int amount) {
        integerStream = Stream.
                iterate(3, a -> a + rand.nextInt(100)).limit(amount);

        return integerStream.collect(Collectors.toList());
    }

    public Integer[] generateArray(int amount) {
        integerStream = Stream.
                        generate(() -> rand.nextInt(100)).limit(amount);

        return integerStream.toArray(Integer[]::new);
    }
}
