package com.oleksandr.streams;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        IntGenerator intGenerator = new IntGenerator();
        List<Integer> integerList = intGenerator.generateList(10);
        System.out.println(integerList);

        Integer[] integers = intGenerator.generateArray(10);
        for (Integer elem : integers) {
            System.out.print(elem + " ");
        }

        IntStreamWorker intStreamWorker = new IntStreamWorker(integerList);
        intStreamWorker.countCharacteristics();
        System.out.println("bigger than avg: " +
                intStreamWorker.countBiggerValues());
    }
}
