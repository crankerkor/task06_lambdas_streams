package com.oleksandr.streams;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class IntStreamWorker {
    private Supplier<Stream<Integer>> integerStreams;

    public IntStreamWorker(List<Integer> integerList) {
        integerStreams = integerList::stream;
    }

    public IntStreamWorker(Integer[] integers) {
        integerStreams = () -> Stream.of(integers);
    }

    public void countCharacteristics() {

        System.out.println("\navg: " +
                average());
        System.out.println("min: " +
                integerStreams.get().min(Comparator.comparingInt(a -> a)).get() +
                "\nmax: " +
                integerStreams.get().max(Comparator.comparingInt(a -> a)).get() +
                "\nsum using reduce: " +
                integerStreams.get().reduce(0, Integer::sum));
    }

    private long average() {
        int sum = (integerStreams.get().mapToInt(a -> a).sum());
       return sum / integerStreams.get().count();
    }

    public long countBiggerValues() {
        long amount = 0;

        amount = integerStreams.get().
                filter(a -> a > average()).
                count();

        return amount;
    }
}
