package com.oleksandr.lambdas;

import com.oleksandr.ConsoleReader;


public class Main {
    public static void main(String[] args) {
        ConsoleReader consoleReader = new ConsoleReader();
//        int x = consoleReader.getInt();
//        int y = consoleReader.getInt();
//        int z = consoleReader.getInt();

        IntInterface maxValue = (a, b, c) -> {
            return a > b ? (a > c ? a : c) : (b > c ? b : c);
        };

        IntInterface avgValue = (a, b, c) -> ((a + b + c) / 3);

        consoleReader.chooseCommand();

        //System.out.println(x + " " + y + " " + z + " " + avgValue.mathFunction(x, y, z));
    }

}
