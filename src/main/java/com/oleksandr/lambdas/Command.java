package com.oleksandr.lambdas;

public interface Command {
    public String changeString(String str);
}
