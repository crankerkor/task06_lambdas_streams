package com.oleksandr.lambdas;

@FunctionalInterface
public interface IntInterface {
   public int mathFunction(int a, int b, int c);
}
