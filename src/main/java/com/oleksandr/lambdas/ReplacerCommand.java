package com.oleksandr.lambdas;

public class ReplacerCommand implements Command {
    public String changeString(String notChangedStr) {
        return notChangedStr.replaceAll("[aAoOuUeEIi]", "");
    }
}
