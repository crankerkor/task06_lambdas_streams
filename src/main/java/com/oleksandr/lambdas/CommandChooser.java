package com.oleksandr.lambdas;

public class CommandChooser {

    public Command[] initializeCommands() {
        Command[] commandArray = new Command[4];

        Command lower = arg -> arg.toLowerCase();
        commandArray[0] = lower;

        Command upper = String::toUpperCase;
        commandArray[1] = upper;

        Command anonymous = new Command() {
            public String changeString(String notAnon) {
                return notAnon + " you're just like me";
            }
        };
        commandArray[2] = anonymous;

        ReplacerCommand replacer = new ReplacerCommand();
        commandArray[3] = replacer;

        return commandArray;
    }


}
