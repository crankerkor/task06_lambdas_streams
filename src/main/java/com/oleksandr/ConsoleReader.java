package com.oleksandr;

import com.oleksandr.lambdas.Command;
import com.oleksandr.lambdas.CommandChooser;
import com.oleksandr.lambdas.ReplacerCommand;

import java.util.Scanner;

public class ConsoleReader {
    private Scanner scanner;
    private CommandChooser commandChooser;

    public ConsoleReader() {
        commandChooser = new CommandChooser();
    }

    public int getInt() {
        scanner = new Scanner(System.in, "UTF-8");

        System.out.println("Please enter integer number");

        while (!scanner.hasNextInt()) {
            System.out.println("Please enter integer number");
            scanner.next();
        }

        int value = scanner.nextInt();

        return value;
    }

    public void chooseCommand() {
        scanner = new Scanner(System.in, "utf-8");
        String commandName;

        do {

            System.out.println("Choose a command:\n" +
                    "lower = to lower case\n" +
                    "upper = to upper case\n" +
                    "anonymous = and the problem is\n" +
                    "replacer = delete vowels\n" +
                    "stop = stop this circle");

            Command[] commands = commandChooser.initializeCommands();

            commandName = scanner.next();
            String arg = scanner.next();

            switch (commandName) {
                case "lower":
                    System.out.println(commands[0].changeString(arg));
                    break;
                case "upper":
                    System.out.println(commands[1].changeString(arg));
                    break;
                case "anonymous":
                    System.out.println(commands[2].changeString(arg));
                    break;
                case "replacer":
                    System.out.println(commands[3].changeString(arg));
                    break;
                case "stop":
                    break;
                default:
                    System.out.println("There is no such command");
                    break;
            }

        }while(!commandName.equals("stop"));
    }
}
